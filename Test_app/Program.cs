﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Lab2;

namespace Test_app
{
    class Program
    {
        static void BitArrayToByteArrayTest()
        {
            byte[] srcArray = new byte[] { 0xff, 0x00, 0x01, 0x02 };
            BitArray bitArray = new BitArray(srcArray);
            byte[] restoredSrcArray = Utils.BitArrayToByteArray(bitArray);

            Console.WriteLine("src array:");
            foreach (byte b in srcArray)
            {
                Console.Write("{0}, ", b);
            }
            Console.WriteLine("\nbit array:");
            foreach (bool b in bitArray)
            {
                Console.Write("{0}, ", b ? 1 : 0);
            }
            Console.WriteLine("\nrestored array:");
            foreach (byte b in restoredSrcArray)
            {
                Console.Write("{0}, ", b);
            }
            Console.WriteLine();
        }

        static void Main(string[] args)
        {
            byte[] data = new byte[2];
            data[0] = 0xab;
            data[1] = 0xcd;

            byte[] key = new byte[8];
            key[0] = 0xaa;
            key[1] = 0xaa;
            DES des = new DES(1);
            byte[] result = des.Encrypt(data, key);
            /*foreach(byte b in result)
            {
                Console.Write("{0}, ", b);
            }
            Console.WriteLine();*/
            Console.WriteLine("Encrypt:\ndata = {0}\nkey = {1}\nresult = {2}", BitConverter.ToString(data), BitConverter.ToString(key), BitConverter.ToString(result));

            byte[] decrypted = des.Decrypt(result, key);
            Console.WriteLine("Decrypt:\nresult = {0}", BitConverter.ToString(decrypted));
            
            /*DES des = new DES();

            //string text = Console.ReadLine();
            BitArray key = new BitArray(64);
            key[0] = true;
            key[6] = true;

            BitArray data = new BitArray(64);
            data[0] = true;
            data[6] = true;

            var result = des.Encrypt(data, key);

            for (int i = result.Length - 1; i > 0; --i)
                Console.Write(result[i] + " ");

            Console.WriteLine();*/
        }
    }
}
