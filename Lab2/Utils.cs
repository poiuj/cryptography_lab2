﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab2
{
    public static class Utils
    {
        public static BitArray MakePermutation(BitArray data, int[] permutationTable)
        {
            BitArray result = new BitArray(permutationTable.Length);
            for (int index = 0; index < result.Length; ++index)
            {
                result[index] = data[permutationTable[index] - 1];
            }

            return result;
        }

        public static BitArray TakeNBits(BitArray data, int n, int startIndex = 0)
        {
            BitArray result = new BitArray(n);
            for (int i = 0; i < n; ++i)
                result[i] = data[i + startIndex];

            return result;
        }

        public static BitArray ShiftLeft(BitArray data, int k)
        {
            BitArray result = new BitArray(data.Length);

            int i = data.Length - 1;
            for (; i > k; --i)
            {
                result[i] = data[i - k];
            }
            for (; i > 0; --i)
            {
                result[i] = false;
            }

            return result;
        }

        public static BitArray Concat(BitArray a, BitArray b)
        {
            BitArray result = new BitArray(a.Length + b.Length);

            int index = 0;
            for (int i = 0; i < a.Length; ++i)
            {
                result[index] = a[i];
                ++index;
            }
            for (int i = 0; i < b.Length; ++i)
            {
                result[index] = b[i];
                ++index;
            }

            return result;
        }

        public static byte[] BitArrayToByteArray(BitArray src)
        {
            byte[] result = new byte[src.Length / 8];

            for (int byteNumber = 0; byteNumber < src.Length / 8; ++byteNumber)
            {
                byte currByte = 0;

                for (int bytePos = 0; bytePos < 8; ++bytePos)
                {
                    currByte = (byte)(currByte << 1);
                    currByte |= (byte)(src[byteNumber * 8 + 7 - bytePos] ? 1 : 0);
                }

                result[byteNumber] = currByte;
            }

            return result;
        }

        public static byte[] TakeNBytes(byte[] data, int n, int startIndex = 0)
        {
            byte[] result = new byte[n];
            for(int i = 0; i < n; ++i)
            {
                result[i] = data[i + startIndex];
            }

            return result;
        }

        public static byte[] FillBytes(byte[] data, int multiplicity = 8)
        {
            int neededSize = ((int)Math.Ceiling(data.Length / 8.0)) * 8;
            int t = neededSize - data.Length;
            byte lastByte = data[data.Length - 1]; 

            if (t == 0 &&  (lastByte < 1 || lastByte > multiplicity))
                return data;
            if(t == 0)
            {
                neededSize += multiplicity;
                t = 8;
            }

            byte[] result = new byte[neededSize];
            data.CopyTo(result, 0);

            Random rand = new Random();
            int i;
            for(i = 0; i < t - 1; ++i)
            {
                result[data.Length + i] = (byte)rand.Next();
            }
            result[data.Length + i] = (byte)t;

            return result;
        }

        public static byte[] UnfillBytes(byte[] data, int multiplicity = 8)
        {
            byte lastByte = data[data.Length - 1];

            if (lastByte < 1 || lastByte > multiplicity)
                return data;
            return TakeNBytes(data, data.Length - lastByte);
        }
    }
}
