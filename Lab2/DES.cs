﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{

    public class DES
    {
        private Func<BitArray, BitArray> InnerFunc;

        public DES(int mode)
        {
            if (mode == 0)
            {
                InnerFunc = _InnerEncryptFunction1;
            }
            else
            {
                InnerFunc = _InnerEncryptFunction2;
            }

            SMatrix2 = new int[][] { S1S2, S3S4, S5S6, S7S8 };
        }

        private int[] InitialPermutation = {58, 50, 42, 34, 26, 18, 10, 02,
                                            60, 52, 44, 36, 28, 20, 12, 04,
                                            62, 54, 46, 38, 30, 22, 14, 06,
                                            64, 56, 48, 40, 32, 24, 16, 08,
                                            57, 49, 41, 33, 25, 17, 09, 01,
                                            59, 51, 43, 35, 27, 19, 11, 03,
                                            61, 53, 45, 37, 29, 21, 13, 05,  
                                            63, 55, 47, 39, 31, 23, 15, 07};

        private int[] FinalPermutation = {40, 08, 48, 16, 56, 24, 64, 32,
                                          39, 07, 47, 15, 55, 23, 63, 31,
                                          38, 06, 46, 14, 54, 22, 62, 30,
                                          37, 05, 45, 13, 53, 21, 61, 29,
                                          36, 04, 44, 12, 52, 20, 60, 28,
                                          35, 03, 43, 11, 51, 19, 59, 27,
                                          34, 02, 42, 10, 50, 18, 58, 26,
                                          33, 01, 41, 09, 49, 17, 57, 25};

        private int[] ExtensionMatrix = {32, 01, 02, 03, 04, 05,
                                        04, 05, 06, 07, 08, 09,
                                        08, 09, 10, 11, 12, 13,
                                        12, 13, 14, 15, 16, 17,
                                        16, 17, 18, 19, 20, 21,
                                        20, 21, 22, 23, 24, 25,
                                        24, 25, 26, 27, 28, 29,
                                        28, 29, 30, 31, 32, 01};

        private int[][] SMatrix = {new int[] {14, 4, 13, 1, 2, 15, 11, 8, 3, 10, 6, 12, 5, 9, 0, 7,
                          0, 15, 7, 4, 14, 2, 13, 1, 10, 6, 12, 11, 9, 5, 3, 8,
                          4, 1, 14, 8, 13, 6, 2, 11, 15, 12, 9, 7, 3, 10, 5, 0,
                         15, 12, 8, 2, 4, 9, 1, 7, 5, 11, 3, 14, 10, 0, 6, 13},

                        new int[]{15, 1, 8, 14, 6, 11, 3, 4, 9, 7, 2, 13, 12, 0, 5, 10,
                          3, 13, 4, 7, 15, 2, 8, 14, 12, 0, 1, 10, 6, 9, 11, 5,
                          0, 14, 7, 11, 10, 4, 13, 1, 5, 8, 12, 6, 9, 3, 2, 15,
                         13, 8, 10, 1, 3, 15, 4, 2, 11, 6, 7, 12, 0, 5, 14, 9},

                        new int[] {10, 0, 9, 14, 6, 3, 15, 5, 1, 13, 12, 7, 11, 4, 2, 8,
                         13, 7, 0, 9, 3, 4, 6, 10, 2, 8, 5, 14, 12, 11, 15, 1,
                         13, 6, 4, 9, 8, 15, 3, 0, 11, 1, 2, 12, 5, 10, 14, 7,
                          1, 10, 13, 0, 6, 9, 8, 7, 4, 15, 14, 3, 11, 5, 2, 12},

                        new int[]{ 7, 13, 14, 3, 0, 6, 9, 10, 1, 2, 8, 5, 11, 12, 4, 15,
                             13, 8, 11, 5, 6, 15, 0, 3, 4, 7, 2, 12, 1, 10, 14, 9,
                             10, 6, 9, 0, 12, 11, 7, 13, 15, 1, 3, 14, 5, 2, 8, 4,
                              3, 15, 0, 6, 10, 1, 13, 8, 9, 4, 5, 11, 12, 7, 2, 14},

                        new int[]{ 2, 12, 4, 1, 7, 10, 11, 6, 8, 5, 3, 15, 13, 0, 14, 9,
                             14, 11, 2, 12, 4, 7, 13, 1, 5, 0, 15, 10, 3, 9, 8, 6,
                              4, 2, 1, 11, 10, 13, 7, 8, 15, 9, 12, 5, 6, 3, 0, 14,
                             11, 8, 12, 7, 1, 14, 2, 13, 6, 15, 0, 9, 10, 4, 5, 3}, 

                        new int[]{ 12, 1, 10, 15, 9, 2, 6, 8, 0, 13, 3, 4, 14, 7, 5, 11,
                             10, 15, 4, 2, 7, 12, 9, 5, 6, 1, 13, 14, 0, 11, 3, 8,
                              9, 14, 15, 5, 2, 8, 12, 3, 7, 0, 4, 10, 1, 13, 11, 6,
                              4, 3, 2, 12, 9, 5, 15, 10, 11, 14, 1, 7, 6, 0, 8, 13}, 

                        new int[]{ 4, 11, 2, 14, 15, 0, 8, 13, 3, 12, 9, 7, 5, 10, 6, 1,
                             13, 0, 11, 7, 4, 9, 1, 10, 14, 3, 5, 12, 2, 15, 8, 6,
                              1, 4, 11, 13, 12, 3, 7, 14, 10, 15, 6, 8, 0, 5, 9, 2,
                              6, 11, 13, 8, 1, 4, 10, 7, 9, 5, 0, 15, 14, 2, 3, 12}, 

                        new int[]{13, 2, 8, 4, 6, 15, 11, 1, 10, 9, 3, 14, 5, 0, 12, 7,
                            1, 15, 13, 8, 10, 3, 7, 4, 12, 5, 6, 11, 0, 14, 9, 2,
                            7, 11, 4, 1, 9, 12, 14, 2, 0, 6, 10, 13, 15, 3, 5, 8,
                            2, 1, 14, 7, 4, 10, 8, 13, 15, 12, 9, 0, 3, 5, 6, 11}};

        private int[] PMatrix = {16, 07, 20, 21,
                                29, 12, 28, 17,
                                01, 15, 23, 26,
                                05, 18, 31, 10,
                                02, 08, 24, 14,
                                32, 27, 03, 09,
                                19, 13, 30, 06,
                                22, 11, 04, 25};

        private BitArray _MakeInitialPermutation(BitArray data)
        {
            return Utils.MakePermutation(data, InitialPermutation);
        }

        private BitArray _MakeFinalPermutation(BitArray data)
        {
            return Utils.MakePermutation(data, FinalPermutation);
        }

        private BitArray _MakeExtension(BitArray data)
        {
            return Utils.MakePermutation(data, ExtensionMatrix);
        }
        
        private int _GetBit(BitArray data, int n)
        {
            return data.Get(n) ? 1 : 0;
        }

        private BitArray _IntToBitArray(int n, int len = 4)
        {
            BitArray result = new BitArray(len, false);

            int index = 0;
            while (n > 0)
            {
                result.Set(index, (n & 1) == 1);

                ++index;
                n = n >> 1;
            }

            return result;
        }

        private int _GetSIndex(BitArray b)
        {
            int rowIndex = _GetBit(b, 0) * 2 + _GetBit(b, 5);
            int colIndex = _GetBit(b, 1) * 8 + _GetBit(b, 2) * 4 + _GetBit(b, 3) * 2 + _GetBit(b, 4);

            return rowIndex * 16 + colIndex;
        }

        private BitArray _MakeSPermutation(BitArray b, int i)
        {
            int[] s = SMatrix[i];

            int sIndex = _GetSIndex(b);

            return _IntToBitArray(s[sIndex]);
        }

        private int[] S1S2 = new int[4096];
        private int[] S3S4 = new int[4096];
        private int[] S5S6 = new int[4096];
        private int[] S7S8 = new int[4096];

        private int[][] SMatrix2;

        private void _InitS(int[] target, int[] s1, int[] s2)
        {
            for (int i = 0; i < 64; ++i)
                {
                    for(int j = 0; j < 64; ++j)
                    {
                        int ij = (j << 6) + i;
                        target[ij] = (s2[j] << 4) + s1[i];
                    }
                }
        }

        private void _InitS1S8()
        {
            for(int i = 0; i < SMatrix2.Length; ++i)
            {
                _InitS(SMatrix2[i], SMatrix[i * 2], SMatrix[i * 2 + 1]);
            }
        }

        private BitArray _MakeSPermutation2(BitArray b1, BitArray b2, int i)
        {
            int[] s = SMatrix2[i];

            int b1Index = _GetSIndex(b1);
            int b2Index = _GetSIndex(b2);

            int s2Index = (b2Index << 6) + b1Index;

            return _IntToBitArray(s[s2Index], 8);
        }

        private BitArray _MakePPermutation(BitArray data)
        {
            return Utils.MakePermutation(data, PMatrix);
        }

        private BitArray _Copy(BitArray data)
        {
            return Utils.TakeNBits(data, data.Length);
        }

        private BitArray _InnerEncryptFunction1(BitArray b)
        {
            BitArray s = new BitArray(0);
            for (int i = 0; i < 8; ++i)
            {
                BitArray bi = Utils.TakeNBits(b, 6, i * 6);
                BitArray si = _MakeSPermutation(bi, i);

                s = Utils.Concat(s, si);
            }

            return s;
        }

        private BitArray _InnerEncryptFunction2(BitArray b)
        {
            BitArray s = new BitArray(0);
            for(int i = 0; i < 4; ++i)
            {
                BitArray b1 = Utils.TakeNBits(b, 6, i * 6);
                BitArray b2 = Utils.TakeNBits(b, 6, i * 6 + 6);
                BitArray si = _MakeSPermutation2(b1, b2, i);

                s = Utils.Concat(s, si);
            }

            return s;
        }

        private BitArray _EncryptFunction(BitArray data, BitArray key)
        {
            if (data.Length != 32)
                throw new ArgumentException("data must be 32 bit vector");
            if (key.Length != 48)
                throw new ArgumentException("key must be 48 bit vector");

            BitArray extData = _MakeExtension(data);

            BitArray b = _Xor(extData, key);

            BitArray s = InnerFunc(b);

            return _MakePPermutation(s);
        }

        private BitArray _Xor(BitArray a, BitArray b)
        {
            BitArray a_copy = _Copy(a);
            return a_copy.Xor(b);
        }

        public byte[] EncryptBlock(byte[] data, byte[] key)
        {
            return Utils.BitArrayToByteArray(EncryptBlock(new BitArray(data), new BitArray(key)));
        }

        public BitArray EncryptBlock(BitArray data, BitArray key)
        {
            KeyGenerator keyGen = new KeyGenerator(key);
            BitArray[] keys = new BitArray[16];
            for (int i = 0; i < 16; ++i)
            {
                keys[i] = keyGen.GetKey(i);
            }

            return _ProcessBlock(data, keys);
        }

        public BitArray DecryptBlock(BitArray data, BitArray key)
        {
            KeyGenerator keyGen = new KeyGenerator(key);
            BitArray[] keys = new BitArray[16];
            for (int i = 0; i < 16; ++i)
            {
                keys[i] = keyGen.GetKey(15 - i);
            }

            return _ProcessBlock(data, keys);
        }

        private BitArray _ProcessBlock(BitArray data, BitArray[] keys)
        {
            if (data.Length != 64)
                throw new ArgumentException("data must be 64 bit vector");

            BitArray t0 = _MakeInitialPermutation(data);

            BitArray l_prev = Utils.TakeNBits(t0, 32);
            BitArray r_prev = Utils.TakeNBits(t0, 32, 32);

            for (int iteration = 0; iteration < 16; ++iteration)
            {
                BitArray ki = keys[iteration];
                BitArray l = _Copy(r_prev);
                BitArray r = _Xor(l_prev, _EncryptFunction(r_prev, ki));

                l_prev = l;
                r_prev = r;
            }

            BitArray preResult = Utils.Concat(r_prev, l_prev);

            BitArray result = _MakeFinalPermutation(preResult);

            return result;
        }

        public byte[] DecryptBlock(byte[] data, byte[] key)
        {
            return Utils.BitArrayToByteArray(DecryptBlock(new BitArray(data), new BitArray(key)));
        }

        public byte[] Encrypt(byte[] data, byte[] key)
        {
            List<byte> result = new List<byte>();

            byte[] filledData = Utils.FillBytes(data);

            for(int blockNumber = 0; blockNumber < filledData.Length / 8; ++blockNumber)
            {
                byte[] block = Utils.TakeNBytes(filledData, 8, blockNumber * 8);
                result.AddRange(EncryptBlock(block, key));
            }

            return result.ToArray();
        }

        public byte[] Decrypt(byte[] data, byte[] key)
        {
            List<byte> result = new List<byte>();

            for (int blockNumber = 0; blockNumber < data.Length / 8; ++blockNumber)
            {
                byte[] block = Utils.TakeNBytes(data, 8, blockNumber * 8);
                result.AddRange(DecryptBlock(block, key));
            }

            return Utils.UnfillBytes(result.ToArray());
        }
    }
}
