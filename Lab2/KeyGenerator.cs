﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab2
{
    public class KeyGenerator
    {
        private BitArray _initKey;
        private int _iteration;
        private BitArray _c;
        private BitArray _d;

        private int[] GMatrix = {57, 49, 41, 33, 25, 17, 09,
                                01, 58, 50, 42, 34, 26, 18,
                                10, 02, 59, 51, 43, 35, 27,
                                19, 11, 03, 60, 52, 44, 36,
                                63, 55, 47, 39, 31, 23, 15,
                                07, 62, 54, 46, 38, 30, 22,
                                14, 06, 61, 53, 45, 37, 29,
                                21, 13, 05, 28, 20, 12, 04};

        private int[] Shifts = {1,
                                1,
                                2,
                                2,
                                2,
                                2,
                                2,
                                2,
                                1,
                                2,
                                2,
                                2,
                                2,
                                2,
                                2,
                                1};

        private int[] HMatrix = {14, 17, 11, 24, 01, 05,
                                03, 28, 15, 06, 21, 10,
                                23, 19, 12, 04, 26, 08,
                                16, 07, 27, 20, 13, 02,
                                41, 52, 31, 37, 47, 55,
                                30, 40, 51, 45, 33, 48,
                                44, 49, 39, 56, 34, 53,
                                46, 42, 50, 36, 29, 32};

        public BitArray[] _keys = new BitArray[16];

        public BitArray GetKey (int i)
        {
            if (i < 0 || i > 15)
                return null;

            return _keys[i];
        }

        private BitArray _MakeGPermutation(BitArray data)
        {
            return Utils.MakePermutation(data, GMatrix);
        }

        private BitArray _MakeHPermutation(BitArray data)
        {
            return Utils.MakePermutation(data, HMatrix);
        }

        public KeyGenerator(BitArray initKey)
        {
            _initKey = initKey;
            Reset();

            for (int i = 0; i < 16; ++i)
            {
                _keys[i] = _Next();
            }
        }

        public void Reset()
        {
            _iteration = 0;
            _c = _d = null;
        }

        public void _Init()
        {
            BitArray permutedInitKey = _MakeGPermutation(_initKey);

            _c = Utils.TakeNBits(permutedInitKey, 28);
            _d = Utils.TakeNBits(permutedInitKey, 28, 28);
        }

        private BitArray _Next()
        {
            if (_iteration == 0)
                _Init();

            _c = Utils.ShiftLeft(_c, Shifts[_iteration]);
            _d = Utils.ShiftLeft(_d, Shifts[_iteration]);

            BitArray cd = Utils.Concat(_d, _c);

            ++_iteration;

            return _MakeHPermutation(cd);
        }
    }
}
